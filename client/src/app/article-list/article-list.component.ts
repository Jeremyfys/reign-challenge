import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Article } from '../core/beans/article';
import { ArticleService } from '../core/services/article.service';

@Component({
  selector: 'article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent implements OnInit {
  public articles: Article[] = [];
  public todayUTC = this.datePipe.transform(new Date(), 'yyyy-MM-dd', 'UTC')!;

  constructor(
    private articleService: ArticleService,
    private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.getArticles();
  }

  getArticles() {
    this.articleService.getArticles().subscribe(response => {
      this.articles = response;
    })
  }

  openUrl(event: any, article: Article) {
    event.stopPropagation();
    console.log(article);
    window.open(article.story_url ?? article.url, "_blank")
  }

  deleteArticle(event: any, article: Article, index: number) {
    event.stopPropagation();
    this.articleService.deleteArticle(article._id).subscribe(response => {
      this.articles.splice(index, 1);
    });
  }
}
