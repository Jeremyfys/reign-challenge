import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({ name: 'formatArticleDate' })
export class FormatArticleDatePipe implements PipeTransform {
  constructor(private datePipe: DatePipe) { }
  transform(value: number, today: string): string {
    const date = this.datePipe.transform(value * 1000, 'yyyy-MM-dd', 'UTC');
    if (date === today) {
      return this.datePipe.transform(value * 1000, 'shortTime', 'UTC')!
    }

    const yesterday = this.datePipe.transform(new Date(Date.now() - 86400000), 'yyyy-MM-dd', 'UTC')!
    if (date === yesterday) {
      return 'Yesterday';
    }


    return this.datePipe.transform(value * 1000, 'MMM d', 'UTC')!;
  }
}