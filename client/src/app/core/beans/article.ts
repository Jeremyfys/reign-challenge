export interface Article {
  _id: string;
  story_id: number;
  story_title: string;
  story_text: string;
  story_url: string;
  author: string;
  title: string;
  url: string;
  created_at: string;
  created_at_i: number;
  deleted: number;
}