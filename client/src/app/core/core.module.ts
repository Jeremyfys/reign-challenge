import { HttpClientModule } from '@angular/common/http';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { ArticleService } from './services/article.service';

@NgModule({
  imports: [HttpClientModule],
  providers: [
    ArticleService
  ]
})

export class CoreModule {
  constructor(@Optional() @SkipSelf() core: CoreModule) {
    if (core) {
        throw new Error('You shall not run!');
    }
}
}