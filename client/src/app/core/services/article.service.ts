import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { Article } from '../beans/article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(
    private http: HttpClient,
  ) { }

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(`${environment.url}/article`); 
  }

  deleteArticle(id: string): Observable<Article> {
    return this.http.delete<Article>(`${environment.url}/article/${id}`, {}); 
  }
}
