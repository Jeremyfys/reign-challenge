import { HttpModule } from '@nestjs/axios';
import { Module, Global } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';

import config from '../config';

@Global()
@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: async (configService: ConfigType<typeof config>) => {
        const { connection, username, password, host, port, name, authSource, readPreference } = configService.mongo;
        return {
          uri: `${connection}://${host}:${port}`,
          user: username,
          pass: password,
          dbName: name,
          authSource: authSource,
          readPreference: readPreference,
          directConnection: true,
          useNewUrlParser: true,
          
        }
      },
      inject: [config.KEY],
    }),
    HttpModule.register({
        timeout: 5000,
        maxRedirects: 5,
      }),
  ],
  providers: [],
  exports: [MongooseModule, HttpModule],
})
export class DatabaseModule { }