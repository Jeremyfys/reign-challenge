import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { ArticleModule } from './article/article.module';
import config from './config';

@Module({
  imports: [ConfigModule.forRoot({
    envFilePath: '.env',
    load: [config],
    isGlobal: true
  }),
  ScheduleModule.forRoot(),
  DatabaseModule,
  ArticleModule
],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
