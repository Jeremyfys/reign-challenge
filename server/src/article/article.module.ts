import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleController } from './controllers/article.controller';
import { Article, ArticleSchema } from './entities/article.entity';
import { ArticleService } from './services/article.service';


@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      {
        name: Article.name,
        schema: ArticleSchema
      }
    ])
  ], 
  controllers: [ArticleController],
  providers: [ArticleService]
})
export class ArticleModule {}

