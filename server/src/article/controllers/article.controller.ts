import { Controller, Delete, Get, Param, Query } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { ArticleService } from "../services/article.service";

@ApiTags('article')
@Controller('article')
export class ArticleController {
  constructor(private articleService: ArticleService) { }

  @Get()
  getArticles() {
    return this.articleService.getAllArticles();
  }

  @Get('/populateDB')
  populate() {
    return this.articleService.getArticlesAndSave();
  }

  @Delete('/:id')
  delete(@Param('id') id: string) {
    return this.articleService.deleteArticle(id);
  }
}