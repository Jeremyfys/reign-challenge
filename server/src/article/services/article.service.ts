import { HttpService } from "@nestjs/axios";
import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Cron, CronExpression } from "@nestjs/schedule";
import { Model } from "mongoose";
import { EMPTY, expand, firstValueFrom, map, reduce, switchMap } from "rxjs";
import { Article } from "../entities/article.entity";

export interface Paginator {
  hits: Article[],
  nbHits: number,
  page: number,
  nbPages: number,
  hitsPerPage: number,
  query: string,
  params: string,
  processingTimeMS: number
}


@Injectable()
export class ArticleService {
  constructor(@InjectModel(Article.name) private articleModel: Model<Article>,
    private httpService: HttpService,
  ) { }

  @Cron(CronExpression.EVERY_HOUR)
  async scheduleGetLastArticles() {
    await this.getArticlesAndSave(true);
  }

  getAllArticles() {
    return this.articleModel.find({ deleted: 0 }).sort({ created_at_i: -1 }).exec();
  }

  async deleteArticle(id: string) {
    const article = this.articleModel.findByIdAndUpdate(id, { deleted: 1 }, { new: true }).exec();
    if (!article) {
      throw new NotFoundException(`Article #${id} not found`);
    }
    return article;
  }

  getPaginatedArticles(pageNumber: number, lastArticles: boolean) {
    return this.httpService.get(this.getUrl(pageNumber, lastArticles), { timeout: 0 }).pipe(
      map((axiosResponse) => axiosResponse.data),
      switchMap(async (response: Paginator) => {
        await this.bulkSaveArticles(response.hits);
        return response;
      })
    )
  }

  async getArticlesAndSave(lastArticles = false): Promise<number> {
    let pageNumber = 0;
    const articles = await firstValueFrom(this.getPaginatedArticles(pageNumber, lastArticles).pipe(
      expand((paginator: Paginator) => {
        pageNumber++;
        return paginator.nbHits < paginator.hitsPerPage ? EMPTY : this.getPaginatedArticles(pageNumber, lastArticles)
      }),
      reduce((acc, paginator) => acc + paginator.hits.length, 0)
    ));
    return articles;
  }

  async bulkSaveArticles(articles: Article[]) {
    const bulkArticles = [];
    for (const article of articles) {
      const extraProps = {};
      if (!article.title && !article.story_title) {
        extraProps['deleted'] = 1;
      }

      bulkArticles.push({
        updateOne: {
          filter: { story_id: article.story_id },
          update: { ...article, ...extraProps },
          upsert: true
        }
      })
    }

    return (await this.articleModel.bulkWrite(bulkArticles, { ordered: true })).insertedCount;
  }

  getUrl(pageNumber: number, lastArticles: boolean): string {
    let queryLastArticles = '';
    if (lastArticles) {
      const todayInMs = new Date().setUTCHours(0, 0, 0, 0) / 1000;
      queryLastArticles = `&numericFilters=created_at_i>${todayInMs}`;
    }
    return `https://hn.algolia.com/api/v1/search_by_date?query=nodejs&hitsPerPage=50&page=${pageNumber}${queryLastArticles}`;
  }
}