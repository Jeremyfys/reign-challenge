
import { Test, TestingModule } from '@nestjs/testing';
import { ArticleService } from './article.service';
import { MongooseModule } from '@nestjs/mongoose';


import { closeInMongodConnection, rootMongooseTestModule } from '../../,,/../../test/utils/mongo/moongose-test.module';
import { ArticleSchema } from '../entities/article.entity';

describe('SolicitudService', () => {
    let service: ArticleService;
    let mockSolicitudRepository = {};

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                rootMongooseTestModule(),
                MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }]),
            ],
            providers: [ArticleService],
        }).compile();

        service = module.get<ArticleService>(ArticleService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });


    afterAll(async () => {
        await closeInMongodConnection();
    });
});