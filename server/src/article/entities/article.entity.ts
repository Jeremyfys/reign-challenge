import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";


@Schema()
export class Article extends Document {

  @Prop({ type: Number, unique: true })
  story_id: number;

  @Prop()
  story_title: string;

  @Prop()
  story_text: string;

  @Prop()
  story_url: string;

  @Prop()
  author: string;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop({ type: Number })
  created_at_i: number;

  @Prop({ type: Date })
  created_at: Date;

  @Prop({ type: Number, default: 0 })
  deleted: number;
}

export const ArticleSchema = SchemaFactory.createForClass(Article)