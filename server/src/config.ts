import { registerAs } from '@nestjs/config';

export default registerAs('config', () => {
  return {
    mongo: {
        name: 'reign-mongo',
        username: 'root',
        password: 'root',
        port: 27017,
        host: 'mongo',
        connection: 'mongodb',
        authSource: 'admin',
        readPreference: 'primary'  
    },
  };
});